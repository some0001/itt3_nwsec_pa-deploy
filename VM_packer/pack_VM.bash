underlinePrint() {
    UNDERLINE='________________________________________________________________\n'
    echo -e $UNDERLINE
    echo -e $1
    echo -e $UNDERLINE
}

#CONSTANTS

if [[ -z "${DATASTORE_DIR}" ]]; then
    DATASTORE_DIR=/vmfs/volumes/datastore1
else
    DATASTORE_DIR="${DATASTORE_DIR}"
fi

SOURCE_VM_NAME=$1
VM_NAME=$2
ESXI_USER=$3
ESXI_HOST=$4
ESXI_PASS=$5

PBUILD_FILEPATH=packer_files/build.json

#SCRIPT
#Set variables in the build.json file
underlinePrint 'Setting variables for packer build'

sed -i "s|\"SOURCE_VM_NAME\".*|\"SOURCE_VM_NAME\": \"$SOURCE_VM_NAME\",|g" $PBUILD_FILEPATH;
sed -i "s|\"VM_NAME\".*|\"VM_NAME\": \"$VM_NAME\",|g" $PBUILD_FILEPATH;
sed -i "s|\"ESXI_USER\".*|\"ESXI_USER\": \"$ESXI_USER\",|g" $PBUILD_FILEPATH;
sed -i "s|\"ESXI_HOST\".*|\"ESXI_HOST\": \"$ESXI_HOST\",|g" $PBUILD_FILEPATH;
sed -i "s|\"ESXI_PASS\".*|\"ESXI_PASS\": \"$ESXI_PASS\"|g" $PBUILD_FILEPATH;

echo -e '\nVariables set'

#Run packer
underlinePrint 'Packer building VM'

packer build $PBUILD_FILEPATH &&

echo -e '\nVM built'

#Add vagrant box
underlinePrint 'Adding vagrant box'
vagrant box add --name $VM_NAME vagrant_boxes/$VM_NAME &&

echo -e '\nVagrant box added'

#Transfer .vmx from the ESXI to the packer box
underlinePrint 'Transferring .vmx from ESXI to vagrant box'

rm ~/.vagrant.d/boxes/$VM_NAME/0/vmware_desktop/*.vmx &>/dev/null
sshpass -p $ESXI_PASS scp $ESXI_USER@$ESXI_HOST:$DATASTORE_DIR/$VM_NAME/$VM_NAME.vmx ~/.vagrant.d/boxes/$VM_NAME/0/vmware_desktop
sed -i "s|scsi0:0.filename =.*|scsi0:0.filename = \"$VM_NAME-disk1.vmdk\"|g" ~/.vagrant.d/boxes/$VM_NAME/0/vmware_desktop/*.vmx

echo -e '\nVMX transferred'

underlinePrint '\nScript finished'

